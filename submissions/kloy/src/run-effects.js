import { run as cycleRun } from '@cycle/rxjs-run';
import { makeHTTPDriver, makeReduxDriver, makeWSDriver } from './drivers';
import main from './effects';

export default function run(store) {
  const drivers = {
    HTTP: makeHTTPDriver(),
    REDUX: makeReduxDriver(store.store$, store.dispatch),
    WS: makeWSDriver('ws://localhost:4000')
  };

  return cycleRun(main, drivers);
}
