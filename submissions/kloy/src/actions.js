import { selectNextRowId } from './selectors';

export const JEDI_RESPONSE = 'JEDI_RESPONSE';
export const CHANGE_PLANET = 'CHANGE_PLANET';
export const SCROLL = 'SCROLL';

export function changePlanet(planet) {
  return {
    type: CHANGE_PLANET,
    payload: {
      planet
    }
  };
}

export function jediResponse(payload) {
  return function jediResponseThunk(dispatch, getState) {
    const state = getState();
    const rowId = selectNextRowId(state);

    const action = {
      type: JEDI_RESPONSE,
      payload: {
        ...payload,
        rowId
      }
    };

    dispatch(action);
  };
}

export function scroll(direction) {
  return {
    type: SCROLL,
    payload: {
      direction
    }
  };
}
