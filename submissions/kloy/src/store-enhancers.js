import Rx from 'rxjs';

function observableMagic(
    createStore,
    reducer,
    initialState
) {
    const store = createStore(reducer, initialState);
    const store$ = new Rx.Subject();

    function dispatch(action) {
        const dispatched = store.dispatch(action);
        const state = store.getState();
        store$.next({action, state});
        return dispatched;
    }

    return {
        ...store,
        dispatch,
        store$: store$.share()
    };
}

export const observableEnhancer = createStore => (reducer, initialState) => (observableMagic(
    createStore,
    reducer,
    initialState
));
