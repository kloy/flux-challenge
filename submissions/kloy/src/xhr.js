// Barebones implementation for ajax. No guard rails in place, so make sure to not make mistakes.
import Rx from 'rxjs';
import _ from 'lodash';

export default function xhr(config = {}) {
  const { url, method, headers = {}, timeout = 0, responseType = 'json' } = config;

  const response$ = Rx.Observable.create(function xhrObservable(subscriber) {
    const xhr = new XMLHttpRequest();

    function onLoad() {
      const response = {
        status: xhr.status,
        data: xhr.response,
        request: config,
        error: false
      };

      subscriber.next(response);
      subscriber.complete();
    }

    // Abort is not important and should be ignored. Just call complete to clean up.
    function onAbort() {
      subscriber.complete();
    }

    function onError() {
      const response = {
        status: xhr.status,
        data: xhr.response,
        request: config,
        error: true,
        errorType: 'error'
      };

      subscriber.error(response);
    }

    function onTimeout() {
      const response = {
        request: config,
        error: true,
        errorType: 'timeout'
      };

      subscriber.error(response);
    }

    xhr.open(method, url, true);
    xhr.timeout = timeout;
    xhr.responseType = responseType;

    _.forOwn(headers, function headerCallback(headerValue, headerName) {
          xhr.setRequestHeader(headerName, headerValue);
    });

    xhr.addEventListener('load', onLoad);
    xhr.addEventListener('abort', onAbort);
    xhr.addEventListener('error', onError);
    xhr.addEventListener('timeout', onTimeout);
    xhr.send('');

    return function unsubscribeXhr() {
      xhr.abort();
    };
  });

  Object.defineProperty(response$, 'request', {
    value: config
  });

  return response$;
}
