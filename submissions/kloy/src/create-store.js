import { createStore as createReduxStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { observableEnhancer } from './store-enhancers';
import rootReducer from './reducers';

export default function createStore() {
  // Setup redux
  const initialState = {};
  const enhancers = [
    applyMiddleware(thunk),
    observableEnhancer
  ];

  if (window.devToolsExtension) {
    enhancers.push(window.devToolsExtension());
  }

  const store = createReduxStore(rootReducer, initialState, compose(...enhancers));

  return store;
}
