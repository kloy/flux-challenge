import _ from 'lodash';

export function selectPlanet(state) {
  return state.planet;
}

export function selectJedisForRows(state) {
  const { jedis, rowsToJedis } = state;

  return _.values(rowsToJedis).map(jediId => {
    if (jediId === null) {
      return null;
    }

    return jedis[jediId];
  });
}

export function selectHasJediOnPlanet(state) {
  const planet = selectPlanet(state);
  const jedis = selectJedisForRows(state);
  let hasJediOnPlanet = false;

  jedis.every(jedi => {
    if (jedi && jedi.homeworld.id === planet.id) {
      hasJediOnPlanet = true;
      return false;
    }

    return true;
  });

  return hasJediOnPlanet;
}

export function selectHasNextJedi(state) {
  const jedis = selectJedisForRows(state);
  const jedi = _.last(jedis);

  return jedi && jedi.apprentice.id !== null;
}

export function selectHasPreviousJedi(state) {
  const jedis = selectJedisForRows(state);
  const jedi = jedis[0];

  return jedi && jedi.master.id !== null;
}

export function selectNextRowId(state) {
  const { lastScrollDirection, rowsToJedis } = state;

  let rowsKeys = Object.keys(rowsToJedis);
  if (lastScrollDirection === 'up') {
    rowsKeys = rowsKeys.reverse();
  }

  let nextRowId;

  rowsKeys.every(key => {
    const jediId = rowsToJedis[key];

    if (jediId === null) {
      nextRowId = key;
      return false;
    }

    return true;
  });

  return nextRowId;
}

export function selectNextJedi(state) {
  const direction = state.lastScrollDirection;
  const jedis = selectJedisForRows(state).filter(jedi => !!jedi);

  return direction === 'down' ? _.last(jedis).apprentice : jedis[0].master;
}

export function selectHasEmptyRow(state) {
  const { lastScrollDirection, rowsToJedis } = state;
  const jediIds = _.values(rowsToJedis);
  const relevantJediIds = lastScrollDirection === 'up' ? _.take(jediIds, 2) : _.takeRight(jediIds, 2);
  const hasEmptyRow = !!relevantJediIds.filter(jediId => jediId === null).length;

  return hasEmptyRow;
}
