import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './app';
import createStore from './create-store';
import runEffects from './run-effects';

// Setup redux
const store = createStore();
// Start cycle effects
runEffects(store);

// Select element in DOM where we will inject our app
const rootEl = document.getElementById('root');

const rootNode = (
  // pass redux store to all child components
  <Provider store={store}>
    <App />
  </Provider>
);

// Render/inject react app
render(rootNode, rootEl);
