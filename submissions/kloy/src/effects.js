import _ from 'lodash';
import Rx from 'rxjs';
import { changePlanet, jediResponse, JEDI_RESPONSE, SCROLL } from './actions';
import { selectNextJedi, selectNextRowId, selectHasEmptyRow } from './selectors';

function planet({ WS }) {
  const planet$ = WS.map(msg => JSON.parse(msg.data));
  const dispatchPlanet$ = planet$.map(changePlanet);

  return {
    REDUX: dispatchPlanet$
  };
}

function jedis({ HTTP, REDUX }) {
  const scroll$ = REDUX.filter(({ action }) => action.type === SCROLL);
  const jediResponse$ = REDUX.filter(({ action }) => action.type === JEDI_RESPONSE);
  const requestJedis$ = Rx.Observable.merge(scroll$, jediResponse$)
    .filter(({ state }) => selectHasEmptyRow(state))
    .map(({ state }) => {
      const jedi = selectNextJedi(state);
      const rowId = selectNextRowId(state);

      return { jedi, rowId };
    })
    .filter(({ jedi, rowId }) => !!jedi.url || !_.isString(rowId))
    .map(({ jedi }) => ({
      method: 'GET',
      url: jedi.url
    }))
    .startWith({
      method: 'GET',
      url: 'http://localhost:3000/dark-jedis/3616'
    });

    const dispatchResponse$ = HTTP
      .switchMap(response$ => response$)
      .map(response => jediResponse(response.data));

    return {
      HTTP: requestJedis$,
      REDUX: dispatchResponse$
    };
}

export default function main(sources) {
  const planetSinks = planet(sources);
  const jedisSinks = jedis(sources);

  const sinks = {
    HTTP: jedisSinks.HTTP,
    REDUX: Rx.Observable.merge(
      planetSinks.REDUX,
      jedisSinks.REDUX
    )
  };

  return sinks;
}
