import _ from 'lodash';
import { combineReducers } from 'redux';
import { CHANGE_PLANET, JEDI_RESPONSE, SCROLL } from './actions';

function jedis(state = {}, action) {
  if (action.type !== JEDI_RESPONSE) {
    return state;
  }

  return {
    ...state,
    [`${action.payload.id}`]: action.payload
  };
}

function lastScrollDirection(state = 'down', action) {
  return action.type === SCROLL ? action.payload.direction : state;
}

const initialPlanet = {
  id: null,
  name: ''
};

function planet(state = initialPlanet, action) {
  if (action.type !== CHANGE_PLANET) {
    return state;
  }

  return _.pick(action.payload.planet, ['id', 'name']);
}

const rowKeys = ['0', '1', '2', '3', '4'];
const initialRowValues = rowKeys.map(() => null);
const initialRowToJedisState = _.zipObject(rowKeys, initialRowValues);

function rowsToJedis(state = initialRowToJedisState, action) {
  switch (action.type) {
    case SCROLL: {
      const direction = action.payload.direction;
      const jediIds = _.values(state);
      const emptyValues = [null, null];

      const nextValues = (
        direction === 'up'
        ? emptyValues.concat(_.dropRight(jediIds, 2))
        : _.drop(jediIds, 2).concat(emptyValues)
      );

      return _.zipObject(rowKeys, nextValues);
    }
    case JEDI_RESPONSE: {
      const { id, rowId } = action.payload;

      return {
        ...state,
        [`${rowId}`]: id
      };
    }
    default:
      return state;
  }
}

export default combineReducers({
  jedis,
  lastScrollDirection,
  planet,
  rowsToJedis
});
