import Rx from 'rxjs';
import xhr from './xhr';

export function makeReduxDriver(store$, dispatch) {
    return function reduxDriver(actions$) {
      actions$.subscribe(action => {
        dispatch(action);
      });

      return store$;
    };
}

export function makeWSDriver(url) {
  return function WSDriver() {
    return Rx.Observable.create(function subscribe(subscriber) {
      function onError(error) {
        subscriber.error(error);
      }

      function onMessage(message) {
        subscriber.next(message);
      }

      const connection = new WebSocket(url);
      connection.onerror = onError;
      connection.onmessage = onMessage;
    }).share();
  };
}

export function makeHTTPDriver() {
  function HTTPDriver(request$) {
    const response$$ = new Rx.Subject();
    let hasSentFirst = false;

    request$.subscribe(config => {
      // HACK: For some reason we have a race condition?
      if (!hasSentFirst) {
        setTimeout(() => {
          const response$ = xhr(config);
          response$$.next(response$);
          hasSentFirst = true;
        }, 50);
      } else {
        const response$ = xhr(config);
        response$$.next(response$);
      }
    });

    return response$$.share();
  }

  return HTTPDriver;
}
