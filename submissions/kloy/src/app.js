import React, { createClass } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import css from 'classname';
import {
  selectPlanet,
  selectJedisForRows,
  selectHasJediOnPlanet,
  selectHasNextJedi,
  selectHasPreviousJedi
} from './selectors';
import { scroll } from './actions';

function buildJediStyle(isHomeWorld) {
  return isHomeWorld ? { color: 'red' } : null;
}

const buttonUpClassNames = isDisabled => css('css-button-up', {
  'css-button-disabled': isDisabled
});

const buttonDownClassNames = isDisabled => css('css-button-down', {
  'css-button-disabled': isDisabled
});

const App = createClass({
  displayName: 'App',

  onScrollUp() {
    this.props.scroll('up');
  },

  onScrollDown() {
    this.props.scroll('down');
  },

  renderJedis() {
    const { jedis, planet } = this.props;

    return jedis.map((jedi, index) => {
      if (!jedi) {
        return <li className="css-slot" key={index} />;
      }

      const isHomeWorld = planet.id === jedi.homeworld.id;
      const jediStyles = buildJediStyle(isHomeWorld);

      return (
        <li className="css-slot" key={index} style={jediStyles}>
          <h3>{jedi.name}</h3>
          <h6>Homeworld: {jedi.homeworld.name}</h6>
        </li>
      );
    });
  },

  render() {
    const { hasJediOnPlanet, planet, hasNextJedi, hasPreviousJedi } = this.props;
    const jedisNodes = this.renderJedis();
    const isScrollUpDisabled = hasJediOnPlanet || !hasPreviousJedi;
    const isScrollDownDisabled = hasJediOnPlanet || !hasNextJedi;

    return (
      <div className="app-container">
        <div className="css-root">
          <h1 className="css-planet-monitor">Obi-Wan currently on {planet.name}</h1>

          <section className="css-scrollable-list">
            <ul className="css-slots">
              {jedisNodes}
            </ul>

            <div className="css-scroll-buttons">
              <button
                className={buttonUpClassNames(isScrollUpDisabled)}
                disabled={isScrollUpDisabled}
                onClick={this.onScrollUp}
              />
              <button
                className={buttonDownClassNames(isScrollDownDisabled)}
                disabled={isScrollDownDisabled}
                onClick={this.onScrollDown}
              />
            </div>
          </section>
        </div>
      </div>
    );
  }
});

const selector = createSelector(
  selectHasJediOnPlanet,
  selectJedisForRows,
  selectPlanet,
  selectHasNextJedi,
  selectHasPreviousJedi,
  function combineProps(hasJediOnPlanet, jedis, planet, hasNextJedi, hasPreviousJedi) {
    return {
      hasJediOnPlanet,
      jedis,
      planet,
      hasNextJedi,
      hasPreviousJedi
    };
  }
);

const actions = {
  scroll
};

export default connect(selector, actions)(App);
